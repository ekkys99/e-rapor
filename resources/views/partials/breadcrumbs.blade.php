<?php
$url = request()->getPathInfo(); //amil path direktorinya
$items = explode('/', $url);
unset($items[0]);
?>
<nav>
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.index') }}">Home</a></li>
        @foreach ($items as $key => $item)
            @if ($key == count($items))
                <li class="breadcrumb-item active">{{ Str::ucfirst($item) }}</li>
            @else
                <li class="breadcrumb-item "><a href="/{{ $item }}">{{ Str::ucfirts($item) }}</a></li>
            @endif
        @endforeach
    </ol>
</nav>
