<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;

    protected $fillable = [
        //data pribadi
        'status_siswa',
        'tgl_kelulusan',
        'nisn',
        'nik',
        'no_akta',
        'nama_siswa',
        'jenis_kelamin',
        'tempat_lahir',
        'tgl_lahir',
        'agama',
        'no_telpon_siswa',
        'jarak_rumah',
        'alat_transportasi',
        //data ayah
        'nama_ayah',
        'tempat_lahir_ayah',
        'tgl_lahir_ayah',
        'pendidikan_ayah',
        'pekerjaan_ayah',
        'penghasilan_ayah',
        //data ibu
        'nama_ibu',
        'tempat_lahir_ibu',
        'tgl_lahir_ibu',
        'pendidikan_ibu',
        'pekerjaan_ibu',
        'penghasilan_ibu',
        //data wali
        'nama_wali',
        'tempat_lahir_wali',
        'tgl_lahir_wali',
        'pendidikan_wali',
        'pekerjaan_wali',
        'penghasilan_wali',
        'jenis_tempat_tinggal',
        'provinsi',
        'kabupaten',
        'kecamatan',
        'kode_pos',
        'alamat'
    ];
}
