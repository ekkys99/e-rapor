<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //menampilkan  all siswa
        $siswas = Siswa::latest()->get();
        return view('contents.siswa.list')->with([
            'siswas' => $siswas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contents.siswa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validasi inputan
        $this->validate($request, [
            //data pribadi
            'status_siswa' => 'required',
            'tgl_kelulusan' => 'required',
            'nisn' => 'required',
            'nik' => 'required',
            'no_akta' => 'required',
            'nama_siswa' => 'required',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'agama' => 'required',
            'no_telpon_siswa' => 'required',
            'jarak_rumah' => 'required',
            'alat_transportasi' => 'required',
            //data ayah
            'nama_ayah' => 'required',
            'tempat_lahir_ayah' => 'required',
            'tgl_lahir_ayah' => 'required',
            'pendidikan_ayah' => 'required',
            'pekerjaan_ayah' => 'required',
            'penghasilan_ayah' => 'required',
            //data ibu
            'nama_ibu' => 'required',
            'tempat_lahir_ibu' => 'required',
            'tgl_lahir_ibu' => 'required',
            'pendidikan_ibu' => 'required',
            'pekerjaan_ibu' => 'required',
            'penghasilan_ibu' => 'required',
            //data wali
            'nama_wali' => 'required',
            'tempat_lahir_wali' => 'required',
            'tgl_lahir_wali' => 'required',
            'pendidikan_wali' => 'required',
            'pekerjaan_wali' => 'required',
            'penghasilan_wali' => 'required',
            'jenis_tempat_tinggal' => 'required',
            'provinsi' => 'required',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'kode_pos' => 'required',
            'alamat' => 'required',
        ]);

        //proses save data
        $storeSiswa = Siswa::create([
            //data pribadi
            'status_siswa' => $request->status_siswa,
            'tgl_kelulusan' => $request->tgl_kelulusan,
            'nisn' => $request->nisn,
            'nik' => $request->nik,
            'no_akta' => $request->no_akta,
            'nama_siswa' => $request->nama_siswa,
            'jenis_kelamin' => $request->jenis_kelamin,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'agama' => $request->agama,
            'no_telpon_siswa' => $request->no_telpon_siswa,
            'jarak_rumah' => $request->jarak_rumah,
            'alat_transportasi' => $request->alat_transportasi,
            //data ayah
            'nama_ayah' => $request->nama_ayah,
            'tempat_lahir_ayah' => $request->tempat_lahir_ayah,
            'tgl_lahir_ayah' => $request->tgl_lahir_ayah,
            'pendidikan_ayah' => $request->pendidikan_ayah,
            'pekerjaan_ayah' => $request->pekerjaan_ayah,
            'penghasilan_ayah' => $request->penghasilan_ayah,
            //data ibu
            'nama_ibu' => $request->nama_ibu,
            'tempat_lahir_ibu' => $request->tempat_lahir_ibu,
            'tgl_lahir_ibu' => $request->tgl_lahir_ibu,
            'pendidikan_ibu' => $request->pendidikan_ibu,
            'pekerjaan_ibu' => $request->pekerjaan_ibu,
            'penghasilan_ibu' => $request->penghasilan_ibu,
            //data wali
            'nama_wali' => $request->nama_wali,
            'tempat_lahir_wali' => $request->tempat_lahir_wali,
            'tgl_lahir_wali' => $request->tgl_lahir_wali,
            'pendidikan_wali' => $request->pendidikan_wali,
            'pekerjaan_wali' => $request->pekerjaan_wali,
            'penghasilan_wali' => $request->penghasilan_wali,
            'jenis_tempat_tinggal' => $request->jenis_tempat_tinggal,
            'provinsi' => $request->provinsi,
            'kabupaten' => $request->kabupaten,
            'kecamatan' => $request->kecamatan,
            'kode_pos' => $request->kode_pos,
            'alamat' => $request->kode_pos,
        ]);

        // dd($storeSiswa);

        //kondisi jika save berhasil atau tidak
        if ($storeSiswa) {
            return redirect()
                ->route('siswa.index')
                ->with([
                    'success' =>  "Berhasil menyimpan data siswa !"
                ]);
        } else {
            return redirect()
                ->back()
                ->withInput()
                ->with([
                    'error' => 'Gagal menyimpan data siswa!'
                ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function show(Siswa $siswa)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);
        return view('contents.siswa.edit', compact('siswa'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // validasi inputan
        $this->validate($request, [
            //data pribadi
            'status_siswa' => 'required',
            'tgl_kelulusan' => 'required',
            'nisn' => 'required',
            'nik' => 'required',
            'no_akta' => 'required',
            'nama_siswa' => 'required',
            'jenis_kelamin' => 'required',
            'tempat_lahir' => 'required',
            'tgl_lahir' => 'required',
            'agama' => 'required',
            'no_telpon_siswa' => 'required',
            'jarak_rumah' => 'required',
            'alat_transportasi' => 'required',
            //data ayah
            'nama_ayah' => 'required',
            'tempat_lahir_ayah' => 'required',
            'tgl_lahir_ayah' => 'required',
            'pendidikan_ayah' => 'required',
            'pekerjaan_ayah' => 'required',
            'penghasilan_ayah' => 'required',
            //data ibu
            'nama_ibu' => 'required',
            'tempat_lahir_ibu' => 'required',
            'tgl_lahir_ibu' => 'required',
            'pendidikan_ibu' => 'required',
            'pekerjaan_ibu' => 'required',
            'penghasilan_ibu' => 'required',
            //data wali
            'nama_wali' => 'required',
            'tempat_lahir_wali' => 'required',
            'tgl_lahir_wali' => 'required',
            'pendidikan_wali' => 'required',
            'pekerjaan_wali' => 'required',
            'penghasilan_wali' => 'required',
            'jenis_tempat_tinggal' => 'required',
            'provinsi' => 'required',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'kode_pos' => 'required',
            'alamat' => 'required'
        ]);

        $siswaUpdate = Siswa::findOrFail($id);

        $siswaUpdate->update([
            //data pribadi
            'status_siswa' => $request->status_siswa,
            'tgl_kelulusan' => $request->tgl_kelulusan,
            'nisn' => $request->nisn,
            'nik' => $request->nik,
            'no_akta' => $request->no_akta,
            'nama_siswa' => $request->nama_siswa,
            'jenis_kelamin' => $request->jenis_kelamin,
            'tempat_lahir' => $request->tempat_lahir,
            'tgl_lahir' => $request->tgl_lahir,
            'agama' => $request->agama,
            'no_telpon_siswa' => $request->no_telpon_siswa,
            'jarak_rumah' => $request->jarak_rumah,
            'alat_transportasi' => $request->alat_transportasi,
            //data ayah
            'nama_ayah' => $request->nama_ayah,
            'tempat_lahir_ayah' => $request->tempat_lahir_ayah,
            'tgl_lahir_ayah' => $request->tgl_lahir_ayah,
            'pendidikan_ayah' => $request->pendidikan_ayah,
            'pekerjaan_ayah' => $request->pekerjaan_ayah,
            'penghasilan_ayah' => $request->penghasilan_ayah,
            //data ibu
            'nama_ibu' => $request->nama_ibu,
            'tempat_lahir_ibu' => $request->tempat_lahir_ibu,
            'tgl_lahir_ibu' => $request->tgl_lahir_ibu,
            'pendidikan_ibu' => $request->pendidikan_ibu,
            'pekerjaan_ibu' => $request->pekerjaan_ibu,
            'penghasilan_ibu' => $request->penghasilan_ibu,
            //data wali
            'nama_wali' => $request->nama_wali,
            'tempat_lahir_wali' => $request->tempat_lahir_wali,
            'tgl_lahir_wali' => $request->tgl_lahir_wali,
            'pendidikan_wali' => $request->pendidikan_wali,
            'pekerjaan_wali' => $request->pekerjaan_wali,
            'penghasilan_wali' => $request->penghasilan_wali,
            'jenis_tempat_tinggal' => $request->jenis_tempat_tinggal,
            'provinsi' => $request->provinsi,
            'kabupaten' => $request->kabupaten,
            'kecamatan' => $request->kecamatan,
            'kode_pos' => $request->kode_pos,
            'alamat' => $request->kode_pos,
        ]);

        if ($siswaUpdate) {
            return redirect()
                ->route('siswa.index')
                ->with([
                    'success' => 'Data Siswa berhasil di perbarui.'
                ]);
        } else {
            return redirect()
                ->route('siswa.index')
                ->with([
                    'error' => 'Data Siswa gagal di perbarui.'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Siswa  $siswa
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $siswa = Siswa::findOrFail($id);
        $siswa->delete();

        if ($siswa) {
            return redirect()
                ->route('siswa.index')
                ->with([
                    'success' => 'Siswa Berhasil Dihapus !'
                ]);
        } else {
            return redirect()
                ->route('siswa.index')
                ->with([
                    'error' => 'Siswa Gagal Dihapus !'
                ]);
        }
    }
}
