@extends('layouts.main-layout-admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Input Data Guru</h5>
            <hr>
            <form class="row g-3" method="post" action="{{ route('guru.store') }}">
                @csrf
                <!-- Form Guru -->
                <div class="col-md-6">
                    <label for="nip" class="form-label">NIP</label>
                    <input type="text" class="form-control" id="nip" name="nip">
                </div>
                <div class="col-md-6">
                    <label for="nik" class="form-label">NIK</label>
                    <input type="text" class="form-control" id="nik" name="nik">
                </div>
                <div class="col-md-12">
                    <label for="nama_guru" class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" id="nama_guru" name="nama_guru"
                        placeholder="sesuai akta kelahiran">
                </div>
                <div class="col-md-4">
                    <label for="jenis_kelamin" class="form-label">Jenis Kelamin</label>
                    <select id="jenis_kelamin" name="jenis_kelamin" class="form-select">
                        <option selected>- Pilih -</option>
                        <option value="L">Laki-laki</option>
                        <option value="P">Perempuan</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="tempat_lahir" class="form-label">Tempat Lahir</label>
                    <select id="tempat_lahir" name="tempat_lahir" class="form-select">
                        <option selected>- Pilih -</option>
                        <option value="Sidoarjo">Sidoarjo</option>
                        <option value="Mojokerto">Mojokerto</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="tgl_lahir" class="form-label">Tgl Lahir</label>
                    <input type="date" class="form-control" name="tgl_lahir">
                </div>
                <div class="col-md-6">
                    <label for="agama" class="form-label">Agama</label>
                    <select id="agama" class="form-select" name="agama">
                        <option>- Pilih -</option>
                        <option value="Islam">Islam</option>
                        <option value="Kristen Katolik">Kristen Katolik</option>
                        <option value="Kristen Protestan">Kristen Protestan</option>
                        <option value="Hindu">Hindu</option>
                        <option value="Budha">Budha</option>
                        <option value="Konghucu">Konghucu</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="no_telpon_siswa" class="form-label">No. Telpon/Hp Siswa</label>
                    <input type="text" class="form-control" id="no_telpon_siswa" name="no_telpon_siswa"
                        placeholder="089606769465">
                </div>
                <div class="col-md-6">
                    <label for="jarak_rumah" class="form-label">Jarak dari rumah ke sekolah (KM)</label>
                    <input type="text" class="form-control" id="jarak_rumah" name="jarak_rumah">
                </div>
                <div class="col-md-6">
                    <label for="alat_transportasi" class="form-label">Alat Transportasi</label>
                    <select id="alat_transportasi" class="form-select" name="alat_transportasi">
                        <option>- Pilih -</option>
                        <option value="Naik kendaraan umum">Naik kendaraan umum</option>
                        <option value="Naik kendaraan pribadi">Naik kendaraan pribadi</option>
                    </select>
                </div>
                <div class="text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
            </form>
        </div>
    </div>
@endsection
