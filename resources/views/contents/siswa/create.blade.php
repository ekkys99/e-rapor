@extends('layouts.main-layout-admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Input Data Pribadi Siswa</h5>
            <hr>
            <form class="row g-3" method="post" action="{{ route('siswa.store') }}">
                @csrf
                <!-- Form Siswa -->
                <div class="col-md-6">
                    <label for="status_siswa" class="form-label">Status Siswa</label>
                    <select id="status_siswa" name="status_siswa" class="form-select">
                        <option selected>- Pilih -</option>
                        <option value="aktif">aktif</option>
                        <option value="nonaktif">nonaktif</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="tgl_kelulusan" class="form-label">Tgl Kelulusan</label>
                    <input type="date" class="form-control" name="tgl_kelulusan">
                </div>
                <div class="col-md-4">
                    <label for="nisn" class="form-label">NISN</label>
                    <input type="text" class="form-control" id="nisn" name="nisn">
                </div>
                <div class="col-md-4">
                    <label for="nik" class="form-label">NIK</label>
                    <input type="text" class="form-control" id="nik" name="nik">
                </div>
                <div class="col-md-4">
                    <label for="no_akta" class="form-label">Nomor Akta</label>
                    <input type="text" class="form-control" id="no_akta" name="no_akta">
                </div>
                <div class="col-md-12">
                    <label for="nama_siswa" class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" id="nama_siswa" name="nama_siswa"
                        placeholder="sesuai akta kelahiran">
                </div>
                <div class="col-md-4">
                    <label for="jenis_kelamin" class="form-label">Jenis Kelamin</label>
                    <select id="jenis_kelamin" name="jenis_kelamin" class="form-select">
                        <option selected>- Pilih -</option>
                        <option value="L">Laki-laki</option>
                        <option value="P">Perempuan</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="tempat_lahir" class="form-label">Tempat Lahir</label>
                    <select id="tempat_lahir" name="tempat_lahir" class="form-select">
                        <option selected>- Pilih -</option>
                        <option value="Sidoarjo">Sidoarjo</option>
                        <option value="Mojokerto">Mojokerto</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="tgl_lahir" class="form-label">Tgl Lahir</label>
                    <input type="date" class="form-control" name="tgl_lahir">
                </div>
                <div class="col-md-6">
                    <label for="agama" class="form-label">Agama</label>
                    <select id="agama" class="form-select" name="agama">
                        <option>- Pilih -</option>
                        <option value="Islam">Islam</option>
                        <option value="Kristen Katolik">Kristen Katolik</option>
                        <option value="Kristen Protestan">Kristen Protestan</option>
                        <option value="Hindu">Hindu</option>
                        <option value="Budha">Budha</option>
                        <option value="Konghucu">Konghucu</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="no_telpon_siswa" class="form-label">No. Telpon/Hp Siswa</label>
                    <input type="text" class="form-control" id="no_telpon_siswa" name="no_telpon_siswa"
                        placeholder="089606769465">
                </div>
                <div class="col-md-6">
                    <label for="jarak_rumah" class="form-label">Jarak dari rumah ke sekolah (KM)</label>
                    <input type="text" class="form-control" id="jarak_rumah" name="jarak_rumah">
                </div>
                <div class="col-md-6">
                    <label for="alat_transportasi" class="form-label">Alat Transportasi</label>
                    <select id="alat_transportasi" class="form-select" name="alat_transportasi">
                        <option>- Pilih -</option>
                        <option value="Naik kendaraan umum">Naik kendaraan umum</option>
                        <option value="Naik kendaraan pribadi">Naik kendaraan pribadi</option>
                    </select>
                </div>
                <!-- End Form Siswa-->

                <!-- Form Ayah -->
                <h5 class="card-title">Data Ayah</h5>
                <hr>
                <div class="col-md-12">
                    <label for="nama_ayah" class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" id="nama_ayah" name="nama_ayah"
                        placeholder="sesuai akta kelahiran">
                </div>
                <div class="col-md-4">
                    <label for="tempat_lahir_ayah" class="form-label">Tempat Lahir</label>
                    <select id="tempat_lahir_ayah" name="tempat_lahir_ayah" class="form-select">
                        <option>- Pilih -</option>
                        <option value="Sidoarjo">Sidoarjo</option>
                        <option value="Mojokerto">Mojokerto</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="tgl_lahir_ayah" class="form-label">Tgl Lahir</label>
                    <input type="date" class="form-control" name="tgl_lahir_ayah">
                </div>
                <div class="col-md-4">
                    <label for="pendidikan_ayah" class="form-label">Pendidikan Ayah</label>
                    <input type="text" class="form-control" id="pendidikan_ayah" name="pendidikan_ayah">
                </div>
                <div class="col-md-6">
                    <label for="pekerjaan_ayah" class="form-label">Pekerjaan Ayah</label>
                    <input type="text" class="form-control" id="pekerjaan_ayah" name="pekerjaan_ayah">
                </div>
                <div class="col-md-6">
                    <label for="penghasilan_ayah" class="form-label">Penghasilan (Rp)</label>
                    <input type="text" class="form-control" id="penghasilan_ayah" name="penghasilan_ayah"
                        placeholder="contoh : 3500000">
                </div>
                <!-- End Form Ayah-->

                <!-- Form Ibu -->
                <h5 class="card-title">Data Ibu</h5>
                <hr>
                <div class="col-md-12">
                    <label for="nama_ibu" class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" id="nama_ibu" name="nama_ibu"
                        placeholder="sesuai akta kelahiran">
                </div>
                <div class="col-md-4">
                    <label for="tempat_lahir_ibu" class="form-label">Tempat Lahir</label>
                    <select id="tempat_lahir_ibu" name="tempat_lahir_ibu" class="form-select">
                        <option>- Pilih -</option>
                        <option value="Sidoarjo">Sidoarjo</option>
                        <option value="Mojokerto">Mojokerto</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="tgl_lahir_ibu" class="form-label">Tgl Lahir</label>
                    <input type="date" class="form-control" name="tgl_lahir_ibu">
                </div>
                <div class="col-md-4">
                    <label for="pendidikan_ibu" class="form-label">Pendidikan ibu</label>
                    <input type="text" class="form-control" id="pendidikan_ibu" name="pendidikan_ibu">
                </div>
                <div class="col-md-6">
                    <label for="pekerjaan_ibu" class="form-label">Pekerjaan Ibu</label>
                    <input type="text" class="form-control" id="pekerjaan_ibu" name="pekerjaan_ibu">
                </div>
                <div class="col-md-6">
                    <label for="penghasilan_ibu" class="form-label">Penghasilan (Rp)</label>
                    <input type="text" class="form-control" id="penghasilan_ibu" name="penghasilan_ibu"
                        placeholder="contoh : 3500000">
                </div>
                <!-- End Form Ibu-->

                <!-- Form Wali -->
                <h5 class="card-title">Data Wali</h5>
                <hr>
                <div class="col-md-12">
                    <label for="nama_wali" class="form-label">Nama Lengkap</label>
                    <input type="text" class="form-control" id="nama_wali" name="nama_wali"
                        placeholder="sesuai akta kelahiran">
                </div>
                <div class="col-md-4">
                    <label for="tempat_lahir_wali" class="form-label">Tempat Lahir</label>
                    <select id="tempat_lahir_wali" name="tempat_lahir_wali" class="form-select">
                        <option selected>- Pilih -</option>
                        <option value="Sidoarjo">Sidoarjo</option>
                        <option value="Mojokerto">Mojokerto</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="tgl_lahir_wali" class="form-label">Tgl Lahir</label>
                    <input type="date" class="form-control" name="tgl_lahir_wali">
                </div>
                <div class="col-md-4">
                    <label for="pendidikan_wali" class="form-label">Pendidikan wali</label>
                    <input type="text" class="form-control" id="pendidikan_wali" name="pendidikan_wali">
                </div>
                <div class="col-md-6">
                    <label for="pekerjaan_wali" class="form-label">Pekerjaan wali</label>
                    <input type="text" class="form-control" id="pekerjaan_wali" name="pekerjaan_wali">
                </div>
                <div class="col-md-6">
                    <label for="penghasilan_wali" class="form-label">Penghasilan (Rp)</label>
                    <input type="text" class="form-control" id="penghasilan_wali" name="penghasilan_wali"
                        placeholder="contoh : 3500000">
                </div>
                <div class="col-md-4">
                    <label for="jenis_tempat_tinggal" class="form-label">Jenis Tempat Tinggal</label>
                    <select id="jenis_tempat_tinggal" name="jenis_tempat_tinggal" class="form-select">
                        <option>- Pilih -</option>
                        <option value="Sewa">Sewa</option>
                        <option value="Pribadi">Pribadi</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="provinsi" class="form-label">Provinsi</label>
                    <select id="provinsi" name="provinsi" class="form-select">
                        <option>- Pilih -</option>
                        <option value="Jawa Timur">Jawa Timur</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="kabupaten" class="form-label">Kabupaten</label>
                    <select id="kabupaten" name="kabupaten" class="form-select">
                        <option>- Pilih -</option>
                        <option value="Sidoarjo">Sidoarjo</option>
                        <option value="Mojokerto">Mojokerto</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label for="kecamatan" class="form-label">Kecamatan</label>
                    <select id="kecamatan" name="kecamatan" class="form-select">
                        <option>- Pilih -</option>
                        <option value="Krian">Krian</option>

                    </select>
                </div>
                <div class="col-md-6">
                    <label for="kode_pos" class="form-label">Kode Pos</label>
                    <input type="text" class="form-control" id="kode_pos" name="kode_pos">
                </div>
                <div class="col-md-12">
                    <label for="alamat" class="form-label">Alamat</label>
                    <input type="text" class="form-control" id="alamat" name="alamat">
                </div>

                <!-- End Form Wali-->
                <div class="text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
            </form>
        </div>
    </div>
@endsection
