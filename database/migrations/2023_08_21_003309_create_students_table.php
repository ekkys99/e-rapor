<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('status_siswa');
            $table->date('tgl_kelulusan');
            $table->string('nisn');
            $table->string('nik');
            $table->string('no_akta');
            $table->string('nama_siswa');
            $table->string('jenis_kelamin');
            $table->string('tempat_lahir');
            $table->date('tgl_lahir');
            $table->string('agama');
            $table->string('no_telpon_siswa');
            $table->string('jarak_rumah');
            $table->string('alat_transportasi');
            //data ayah
            $table->string('nama_ayah');
            $table->string('tempat_lahir_ayah');
            $table->string('tgl_lahir_ayah');
            $table->string('pendidikan_ayah');
            $table->string('pekerjaan_ayah');
            $table->string('penghasilan_ayah');
            //data ibu
            $table->string('nama_ibu');
            $table->string('tempat_lahir_ibu');
            $table->string('tgl_lahir_ibu');
            $table->string('pendidikan_ibu');
            $table->string('pekerjaan_ibu');
            $table->string('penghasilan_ibu');
            //data wali
            $table->string('nama_wali');
            $table->string('tempat_lahir_wali');
            $table->string('tgl_lahir_wali');
            $table->string('pendidikan_wali');
            $table->string('pekerjaan_wali');
            $table->string('penghasilan_wali');
            $table->string('jenis_tempat_tinggal');
            $table->string('provinsi');
            $table->string('kabupaten');
            $table->string('kecamatan');
            $table->string('kode_pos');
            $table->string('alamat');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
};
