@extends('layouts.main-layout-admin')
@section('content')
    <section class="section">
        <div class="row">
            <div class="col-lg-12">
                <!-- Notifikasi dengan Alert -->
                @if (session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('success') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif

                @if (session('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session('error') }}
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Data Siswa</h5>
                        <a href="{{ route('siswa.create') }}"><button type="button" class="btn btn-success btn-xl"> <i
                                    class="bi-person-plus"></i>
                                Tambah Siswa Baru</button></a>
                        <!-- Table with stripped rows -->
                        <table class="table datatable">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">NISN</th>
                                    <th scope="col">Nama Lengkap</th>
                                    <th scope="col">Jenis Kelamin</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Opsi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $no = 1; ?>
                                @foreach ($siswas as $siswa)
                                    <tr>
                                        <th scope="row">{{ $no++ }}</th>
                                        <td>{{ $siswa->nisn }}</td>
                                        <td>{{ $siswa->nama_siswa }}</td>
                                        <td>{{ $siswa->jenis_kelamin }}</td>
                                        <td>{{ $siswa->status_siswa }}</td>
                                        <td>
                                            <div>
                                                <a href="{{ route('siswa.edit', $siswa->id) }}" class="btn btn-warning"><i
                                                        class="bi bi-pencil"></i></a>
                                                <button type="button" class="btn btn-dark"><i class="bi bi-eye"><a
                                                            href="{{ route('siswa.show', $siswa->id) }}"></a></i></button>

                                                <form onsubmit="return confirm('Apakah Anda Yakin?');"
                                                    action="{{ route('siswa.destroy', $siswa->id) }}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger"><i
                                                            class="bi bi-trash2"></i></button>
                                                </form>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <!-- End Table with stripped rows -->

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
