<?php

namespace App\Http\Controllers;

use App\Models\Kelas;
use Illuminate\Http\Request;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //menampilkan  all kelas
        $kelass = Kelas::latest()->get();
        return view('contents.kelas.list')->with([
            'kelass' => $kelass
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contents.kelas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validasi inputan
        $this->validate($request, [
            'nama_kelas' => 'required',
            'nama_wali_kelas' => 'required',
        ]);

        //save data
        $storeKelas = Kelas::create([
            'nama_kelas' => $request->nama_kelas,
            'nama_wali_kelas' => $request->nama_wali_kelas,
        ]);


        // dd($storeKelas);

        //kondisi jika save berhasil atau tidak
        if ($storeKelas) {
            return redirect()
                ->route('kelas.index')
                ->with([
                    'success' =>  "Berhasil menyimpan data kelas !"
                ]);
        } else {
            return redirect()
                ->back()
                ->withInput()
                ->with([
                    'error' => 'Gagal menyimpan data kelas!'
                ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function show(Kelas $kelas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //menampilkan edit data
        $kelas = Kelas::findOrFail($id);
        return view('contents.kelas.edit', compact('kelas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validasi inputan
        $this->validate($request, [
            'nama_kelas' => 'required',
            'nama_wali_kelas' => 'required',
        ]);

        $kelasUpdate = Kelas::findOrFail($id);

        //udpate data
        $kelasUpdate->update([
            'nama_kelas' => $request->nama_kelas,
            'nama_wali_kelas' => $request->nama_wali_kelas,
        ]);




        // dd($updateKelas);

        //kondisi jika save berhasil atau tidak
        if ($kelasUpdate) {
            return redirect()
                ->route('kelas.index')
                ->with([
                    'success' =>  "Berhasil update data kelas !"
                ]);
        } else {
            return redirect()
                ->back()
                ->withInput()
                ->with([
                    'error' => 'Gagal update data kelas!'
                ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kelas  $kelas
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelas = Kelas::findOrFail($id);
        $kelas->delete();

        if ($kelas) {
            return redirect()
                ->route('kelas.index')
                ->with([
                    'success' => 'Data Kelas Berhasil Dihapus !'
                ]);
        } else {
            return redirect()
                ->route('kelas.index')
                ->with([
                    'error' => 'Data kelas Gagal Dihapus !'
                ]);
        }
    }
}
