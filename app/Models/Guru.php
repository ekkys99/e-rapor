<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guru extends Model
{
    use HasFactory;
    protected $fillable = [
        'nip',
        'nama_guru',
        'jenis_kelamin_guru',
        'almaat_guru',
        'tgl_lahir_guru',
        'no_telpon_guru',
        'jarak_rumah_guru',
        'alat_transportasi',
    ];
}
