@extends('layouts.main-layout-admin')
@section('content')
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Input Data Kelas</h5>
            <hr>
            <form class="row g-3" method="POST" action="{{ route('kelas.store') }}">
                @csrf
                <!-- Form Guru -->
                <div class="col-md-6">
                    <label for="nama_kelas" class="form-label">Nama Kelas</label>
                    <input type="text" class="form-control" id="nama_kelas" name="nama_kelas">
                </div>
                <div class="col-md-6">
                    <label for="nama_wali_kelas" class="form-label">Wali Kelas</label>
                    <input type="text" class="form-control" id="nama_wali_kelas" name="nama_wali_kelas">
                </div>

                <div class="text-center">
                    <button type="submit" class="btn btn-primary">Submit</button>
                    <button type="reset" class="btn btn-secondary">Reset</button>
                </div>
            </form>~
        </div>
    </div>
@endsection
